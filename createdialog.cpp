/** @file createdialog.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "createdialog.h"
#include <QGroupBox>
#include <QBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QRadioButton>
#include <QLineEdit>

CreateDialog::CreateDialog(QString asDefaultName,
                           QList<ShapeChoice> acShapes,
                           QWidget *apParent) :
    QDialog(apParent)
{
    QGroupBox* vpGroupBox = new QGroupBox(tr("Choose new shape:"));

    QVBoxLayout* vpGroupLayout = new QVBoxLayout;

    foreach(ShapeChoice vShapeChoice, acShapes)
    {
        QRadioButton* vpButton = *mcRadioButtons.insert(
                    vShapeChoice.first,
                    new QRadioButton(vShapeChoice.second));
        vpGroupLayout->addWidget(vpButton);
    }
    vpGroupLayout->addStretch();
    mcRadioButtons.values().at(0)->setChecked(true);

    vpGroupBox->setLayout(vpGroupLayout);

    mpName = new QLineEdit(asDefaultName);
    QLabel* vpLabelName = new QLabel(tr("&Name:"));
    vpLabelName->setBuddy(mpName);

    QVBoxLayout* vpVBoxLayout = new QVBoxLayout;
    vpVBoxLayout->addWidget(vpGroupBox);
    vpVBoxLayout->addWidget(vpLabelName);
    vpVBoxLayout->addWidget(mpName);

    QPushButton* vpButtonOk = new QPushButton(tr("&OK"));
    QPushButton* vpButtonCancel = new QPushButton(tr("&Cancel"));

    connect(vpButtonOk, SIGNAL(clicked()), SLOT(accept()));
    connect(vpButtonCancel, SIGNAL(clicked()), SLOT(reject()));

    QHBoxLayout* vpHBoxLayout = new QHBoxLayout;
    vpHBoxLayout->addWidget(vpButtonOk);
    vpHBoxLayout->addWidget(vpButtonCancel);
    vpVBoxLayout->addLayout(vpHBoxLayout);

    setLayout(vpVBoxLayout);

    setWindowTitle(tr("Create dialog"));
}

const QString CreateDialog::choice() const
{
    QString vsResult;
    foreach(QString vsKey, mcRadioButtons.keys())
    {
        if(mcRadioButtons.value(vsKey)->isChecked())
        {
            vsResult = vsKey;
        }
    }
    return vsResult;
}

QString CreateDialog::name() const
{
    return mpName->text();
}
