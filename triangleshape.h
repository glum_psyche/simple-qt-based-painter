/** @file triangleshape.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef TRIANGLESHAPE_H
#define TRIANGLESHAPE_H

#include "baseshape.h"

class TriangleShape : public BaseShape
{
    Q_OBJECT
public:
    TriangleShape(const QString& asName = QString("Triangle"),
                  QColor aTextColor = QColor(Qt::darkGreen),
                  QColor aPenColor = QColor(Qt::darkBlue),
                  QColor aBrushColor = QColor(Qt::white),
                  Qt::PenStyle aPenStyle = Qt::SolidLine,
                  Qt::BrushStyle aBrushStyle = Qt::SolidPattern,
                  int anPenWidth = 1, const QString& asInfo= "",
                  QPointF aCoordinates = QPointF(5, 5),
                  QGraphicsObject* apParent = 0);
    TriangleShape(const TriangleShape& aTriangleShape);
    TriangleShape(const BaseShape& aBaseShape);
    TriangleShape(const ShapeData &aShapeData, QGraphicsObject* apParent = 0);
    ~TriangleShape();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter* apPainter, const QStyleOptionGraphicsItem* apOption, QWidget* );
protected slots:
    virtual void setPath();
private:
    QPolygonF mPath;
};

#endif // TRIANGLESHAPE_H
