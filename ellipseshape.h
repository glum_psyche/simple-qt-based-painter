/** @file ellipseshape.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef ELLIPSESHAPE_H
#define ELLIPSESHAPE_H

#include "baseshape.h"

class EllipseShape : public BaseShape
{
    Q_OBJECT
public:
    EllipseShape(const QString& asName = QString("Ellipse"),
                 QColor aTextColor = QColor(Qt::darkGreen),
                 QColor aPenColor = QColor(Qt::darkBlue),
                 QColor aBrushColor = QColor(Qt::white),
                 Qt::PenStyle aPenStyle = Qt::SolidLine,
                 Qt::BrushStyle aBrushStyle = Qt::SolidPattern,
                 int anPenWidth = 1, const QString& asInfo= "",
                 const QPointF aCoordinates = QPointF(5, 5),
                 QGraphicsObject* apParent = 0);
    EllipseShape(const EllipseShape& aEllipseShape);
    EllipseShape(const BaseShape& aBaseShape);
    EllipseShape(const ShapeData &aShapeData, QGraphicsObject* apParent);
    ~EllipseShape();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter* apPainter, const QStyleOptionGraphicsItem* apOption, QWidget* );
protected slots:
    virtual void setPath();
private:
    QRectF mPath;
};

#endif // ELLIPSESHAPE_H
