/** @file baseshape.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef BASESHAPE_H
#define BASESHAPE_H

#include <QApplication>
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QStyleOptionGraphicsItem>
#include <QPainter>

class QFontMetrics;
class QInputDialog;
class QGraphicsSceneMouseEvent;
class QLabel;
class QMimeData;
class QByteArray;

struct ShapeData
{
    QString msName;
    QString msTypeName;
    QColor mTextColor;
    QColor mBrushColor;
    QColor mPenColor;
    Qt::PenStyle mPenStyle;
    Qt::BrushStyle mBrushStyle;
    int mnPenWidth;
    QString msInfo;
    QPointF mCoordinates;
    int mnZValue;
};
Q_DECLARE_METATYPE(ShapeData)

class BaseShape : public QGraphicsObject
{
    Q_OBJECT
public:
    BaseShape(const QString& asName = QString("Base Figure"),
              const QString& asTypeName = QString("Base Type"),
              const QColor aTextColor = QColor(Qt::darkGreen),
              const QColor aPenColor = QColor(Qt::darkBlue),
              const QColor aBrushColor = QColor(Qt::white),
              const Qt::PenStyle aPenStyle = Qt::SolidLine,
              const Qt::BrushStyle aBrushStyle = Qt::SolidPattern,
              const int anPenWidth = 1,
              const QString& asInfo= "",
              const QPointF aCoordinates = QPointF(5, 5),
              QGraphicsObject* apParent = 0);
    BaseShape(const BaseShape& aBaseShape);
    BaseShape(const ShapeData& aShapeData,
              QGraphicsObject* apParent = 0);

    virtual BaseShape& operator=(const BaseShape& apOther);
    virtual ~BaseShape();

    void setName(const QString& asName);
    void setTextColor(const QColor& aColor);
    void setPenColor(const QColor& aColor);
    void setBrushColor(const QColor& aColor);
    void setPenStyle(const Qt::PenStyle aPenStyle);
    void setBrushStyle(const Qt::BrushStyle aBrushStyle);
    void setPenWidth(const int anPenWidth);
    void setInfo(const QString& asInfo);
    void setCoordinates(const QPointF aCoordinates);
    virtual void deserialize(const QString& asSerialized);

    const QString& name() const;
    const QString& typeName() const;
    const QColor& textColor() const;
    const QColor& penColor() const;
    const QColor& brushColor() const;
    const Qt::PenStyle& penStyle() const;
    const Qt::BrushStyle& brushStyle() const;
    int penWidth() const;
    const QString& info() const;
    const QPointF& coordinates() const;
    virtual QPainterPath shape() const;
    virtual QRectF boundingRect() const;
    virtual const QString serialized() const;
    virtual QMimeData* data() const;

    virtual void putData(const QByteArray& apData);
    virtual void loadFromData(const QMimeData* apData);
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
    void putShape(const ShapeData& aShape);

    static ShapeData toShapeData(const QMimeData* aData);
    static ShapeData toShapeData(const QString& asText);
    QRectF boundingRectGlobal() const;
    void actualizePos();
protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* );
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    QVariant itemChange(GraphicsItemChange aChange, const QVariant& aValue);
    void prepareShape();
protected slots:
    void setCoordinatesOnMove();
    virtual void setPath();
signals:
    void nameChanged();
    void changed();
    void moves(BaseShape* asShape);
    void mouseDoubleClicked(BaseShape*);
protected:
    ShapeData mData;
};

#endif // BASESHAPE_H
