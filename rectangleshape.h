/** @file rectangleshape.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef RECTANGLESHAPE_H
#define RECTANGLESHAPE_H

#include "baseshape.h"

class RectangleShape : public BaseShape
{
    Q_OBJECT
public:
    RectangleShape(const QString& asName = QString("Rectangle"),
                   const QColor aTextColor = QColor(Qt::darkGreen),
                   const QColor aPenColor = QColor(Qt::darkBlue),
                   const QColor aBrushColor = QColor(Qt::white),
                   const Qt::PenStyle aPenStyle = Qt::SolidLine,
                   const Qt::BrushStyle aBrushStyle = Qt::SolidPattern,
                   const int anPenWidth = 1, const QString& asInfo= "",
                   const QPointF aCoordianates = QPointF(5, 5),
                   QGraphicsObject *apParent = 0);
    RectangleShape(const RectangleShape& aRectangleShape);
    RectangleShape(const BaseShape& aBaseShape);
    RectangleShape(const ShapeData &aShapeData, QGraphicsObject* apParent);
    ~RectangleShape();
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter* apPainter, const QStyleOptionGraphicsItem* apOption, QWidget* );
protected slots:
    virtual void setPath();
private:
    QRectF mPath;
};

#endif // RECTANGLESHAPE_H
