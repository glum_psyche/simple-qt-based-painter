/** @file shapefactory.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "shapefactory.h"

ShapeFactory* ShapeFactory::mpInstance = 0;

ShapeFactory::ShapeFactory(QObject *apParent)
    : QObject(apParent)
{
}

void ShapeFactory::destruct()
{
    if(mpInstance)
    {
        delete mpInstance;
        mpInstance = 0;
    }
}

BaseShape *ShapeFactory::createShape(const ShapeData &aData)
{
    BaseShape* vpShape = NULL;
    if(mcShapes.contains(aData.msTypeName))
    {
        vpShape = mcShapes[aData.msTypeName]->createShape(aData);
    }
    return vpShape;
}

BaseShape *ShapeFactory::createShapeFromMimeData(const QMimeData *apMimeData)
{
    ShapeData vData = BaseShape::toShapeData(apMimeData);
    BaseShape* vpShape = NULL;
    if(mcShapes.contains(vData.msTypeName))
    {
        vpShape = mcShapes[vData.msTypeName]->createShape(vData);
    }
    return vpShape;
}

BaseShape *ShapeFactory::createShapeFromSerialized(const QString &asSerialized)
{
    ShapeData vData = BaseShape::toShapeData(asSerialized);
    BaseShape* vpShape = NULL;
    if(mcShapes.contains(vData.msTypeName))
    {
        vpShape = mcShapes[vData.msTypeName]->createShape(vData);
    }
    return vpShape;
}

bool ShapeFactory::isRegistered(const QString &asShapeType) const
{
    return mcShapes.contains(asShapeType);
}

const QStringList ShapeFactory::types() const
{
    return mcShapes.keys();
}

ShapeFactory *ShapeFactory::instance()
{
    if(!mpInstance)
    {
        mpInstance = new ShapeFactory();
    }
    return mpInstance;
}

ShapeFactory::~ShapeFactory()
{
}
