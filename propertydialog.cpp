/** @file propertydialog.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "propertydialog.h"
#include <QGridLayout>
#include <QLabel>

class QPushButton;

PropertyDialog::PropertyDialog(BaseShape *apShape, const QString &asTitle, QWidget *apParent) :
    QDialog(apParent)
{
    mpShape = apShape;
    mpName = new QLineEdit(mpShape->name());
    QLabel* vpName = new QLabel(tr("Name"));
    vpName->setBuddy(mpName);
    mpTextColor = new ColorEdit(mpShape->textColor().name());
    QLabel* vpTextColor = new QLabel(tr("Text color"));
    vpTextColor->setBuddy(mpTextColor);
    mpPenColor = new ColorEdit(mpShape->penColor().name());
    QLabel* vpPenColor = new QLabel(tr("Pen color"));
    vpPenColor->setBuddy(mpPenColor);
    mpBrushColor = new ColorEdit(mpShape->brushColor().name());
    QLabel* vpBrushColor = new QLabel(tr("Brush color"));
    vpBrushColor->setBuddy(mpBrushColor);
    mpPenStyle = new QComboBox;
    mpPenStyle->addItems(QStringList()
                << "NoPen"
                << "SolidLIne"
                << "DashLine"
                << "DotLine"
                << "DashDotLine"
                << "DashDotDotLine"
                << "CustomDashLine");
    QLabel* vpPenStyle = new QLabel(tr("Pen style"));
    vpPenStyle->setBuddy(mpPenStyle);
    mpPenStyle->setCurrentIndex(mpShape->penStyle());
    mpBrushStyle = new QComboBox;
    mpBrushStyle->addItems(QStringList()
                << "NoBrush"
                << "SolidPattern"
                << "Dense1Pattern"
                << "Dense2Pattern"
                << "Dense3Pattern"
                << "Dense4Pattern"
                << "Dense5Pattern"
                << "Dense6Pattern"
                << "Dense7Pattern"
                << "HorPattern"
                << "VerPattern"
                << "CrossPattern"
                << "BDiagPattern"
                << "FDiagPattern"
                << "DiagCrossPattern");
    QLabel* vpBrushStyle = new QLabel(tr("Brush style"));
    vpBrushStyle->setBuddy(mpBrushStyle);
    mpBrushStyle->setCurrentIndex(mpShape->brushStyle());
    mpPenWidth = new QSpinBox;
    QLabel* vpPenWidth = new QLabel(tr("Pen width"));
    vpPenWidth->setBuddy(mpPenWidth);
    mpPenWidth->setValue(mpShape->penWidth());

    QGridLayout* vpGridLayout = new QGridLayout;
    vpGridLayout->addWidget(vpName, 0, 0);
    vpGridLayout->addWidget(mpName, 0, 1);
    vpGridLayout->addWidget(vpTextColor, 0, 2);
    vpGridLayout->addWidget(mpTextColor, 0, 3);
    vpGridLayout->addWidget(vpPenColor, 1, 0);
    vpGridLayout->addWidget(mpPenColor, 1, 1);
    vpGridLayout->addWidget(vpBrushColor, 1, 2);
    vpGridLayout->addWidget(mpBrushColor, 1, 3);
    vpGridLayout->addWidget(vpPenStyle, 2, 0);
    vpGridLayout->addWidget(mpPenStyle, 2, 1);
    vpGridLayout->addWidget(vpBrushStyle, 2, 2);
    vpGridLayout->addWidget(mpBrushStyle, 2, 3);
    vpGridLayout->addWidget(vpPenWidth, 3, 0);
    vpGridLayout->addWidget(mpPenWidth, 3, 1);

    mpInfo = new QLineEdit(mpShape->info());
    QLabel* vpLabelInfo = new QLabel(tr("Information:"));
    vpLabelInfo->setBuddy(mpInfo);

    QPushButton* vpButtonOk = new QPushButton(tr("&OK"));
    QPushButton* vpButtonCancel = new QPushButton(tr("&Cancel"));

    connect(vpButtonOk, SIGNAL(clicked()), SLOT(accept()));
    connect(vpButtonCancel, SIGNAL(clicked()), SLOT(reject()));

    QHBoxLayout* vpHBoxLayout = new QHBoxLayout;
    vpHBoxLayout->addStretch();
    vpHBoxLayout->addWidget(vpButtonOk);
    vpHBoxLayout->addWidget(vpButtonCancel);
    vpHBoxLayout->addStretch();

    QVBoxLayout* vpVBoxLayout = new QVBoxLayout;
    vpVBoxLayout->addLayout(vpGridLayout);
    vpVBoxLayout->addWidget(vpLabelInfo);
    vpVBoxLayout->addWidget(mpInfo);
    vpVBoxLayout->addLayout(vpHBoxLayout);
    setLayout(vpVBoxLayout);

    setWindowTitle(asTitle);
}

void PropertyDialog::accept()
{
    mpShape->setName(mpName->text());
    mpShape->setTextColor(QColor(mpTextColor->text()));
    mpShape->setPenColor(QColor(mpPenColor->text()));
    mpShape->setPenStyle(static_cast<Qt::PenStyle>(mpPenStyle->currentIndex()));
    mpShape->setPenWidth(mpPenWidth->value());
    mpShape->setBrushColor(QColor(mpBrushColor->text()));
    mpShape->setBrushStyle(static_cast<Qt::BrushStyle>(mpBrushStyle->currentIndex()));
    mpShape->setInfo(mpInfo->text());
    QDialog::accept();
}
