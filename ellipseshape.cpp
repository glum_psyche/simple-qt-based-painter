/** @file ellipseshape.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "ellipseshape.h"

EllipseShape::EllipseShape(const QString& asName,
                           QColor aTextColor, QColor aPenColor,
                           QColor aBrushColor, Qt::PenStyle aPenStyle,
                           Qt::BrushStyle aBrushStyle,
                           int anPenWidth, const QString &asInfo,
                           const QPointF aCoordinates, QGraphicsObject *apParent) :
    BaseShape(asName, tr("Ellipse"), aTextColor, aPenColor,
              aBrushColor, aPenStyle, aBrushStyle, anPenWidth,
              asInfo, aCoordinates, apParent)
{
    setPath();
}

EllipseShape::EllipseShape(const EllipseShape &aEllipseShape) :
    BaseShape(aEllipseShape.name(), tr("Ellipse"), aEllipseShape.textColor(),
              aEllipseShape.penColor(), aEllipseShape.brushColor(),
              aEllipseShape.penStyle(), aEllipseShape.brushStyle(),
              aEllipseShape.penWidth(), aEllipseShape.info(),
              aEllipseShape.coordinates(), aEllipseShape.parentObject())
{
    setPath();
}

EllipseShape::EllipseShape(const BaseShape& aBaseShape) :
    BaseShape(aBaseShape)
{
    setPath();
}

EllipseShape::EllipseShape(const ShapeData &aShapeData, QGraphicsObject *apParent) :
    BaseShape(aShapeData, apParent)
{
    setPath();
}

EllipseShape::~EllipseShape()
{
}

QRectF EllipseShape::boundingRect() const
{
    const int vnMargin = 1;
    return mPath.adjusted(-vnMargin, -vnMargin, +vnMargin, +vnMargin);
}

QPainterPath EllipseShape::shape() const
{
    QRectF vRect = mPath;
    QPainterPath vPath;
    vPath.addEllipse(vRect);
    return vPath;
}

void EllipseShape::paint(QPainter *apPainter, const QStyleOptionGraphicsItem *apOption, QWidget *)
{
    apPainter->save();
    QPen vPen(mData.mPenColor);
    if(apOption->state & QStyle::State_Selected)
    {
        vPen.setStyle(Qt::DotLine);
        vPen.setWidth(2);
    }
    else
    {
        vPen.setStyle(penStyle());
        vPen.setWidth(penWidth());
    }
    apPainter->setPen(vPen);
    apPainter->setBrush(QBrush(mData.mBrushColor, mData.mBrushStyle));
    QRectF vRect = mPath;
    apPainter->drawEllipse(vRect);
    apPainter->setPen(mData.mTextColor);
    apPainter->drawText(vRect, Qt::AlignCenter, mData.msName);
    apPainter->restore();
}

void EllipseShape::setPath()
{
    const int vnPadding = 8;
    QFontMetricsF vMetrics = qApp->fontMetrics();
    mPath = vMetrics.boundingRect(mData.msName);
    mPath.adjust(-vnPadding, -vnPadding, +vnPadding, +vnPadding);
    mPath.translate(-mPath.center());
}
