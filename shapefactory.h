/** @file shapefactory.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef SHAPEFACTORY_H
#define SHAPEFACTORY_H

#include "shapebuilder.h"

class QObject;
class QMimeData;

class ShapeFactory : public QObject
{
    Q_OBJECT
public:
    static ShapeFactory* instance();
    static void destruct();

    template <class ShapeClass>
    void registerShape();

    BaseShape* createShape(const ShapeData& aData);
    BaseShape* createShapeFromMimeData(const QMimeData* apMimeData);
    BaseShape* createShapeFromSerialized(const QString& asSerialized);

    bool isRegistered(const QString& asShapeType) const;
    const QStringList types() const;

private:
    ShapeFactory(QObject* apParent = 0);
    ~ShapeFactory();

private:
    static ShapeFactory* mpInstance;

    QMap<QString, AbstractShapeBuilder*> mcShapes;
};

template <class ShapeClass>
void ShapeFactory::registerShape()
{
    AbstractShapeBuilder* vpShape = new ShapeBuilder<ShapeClass>();
    mcShapes.insert(vpShape->shapeType(), vpShape);
}

#endif // SHAPEFACTORY_H
