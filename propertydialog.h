/** @file propertydialog.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef PROPERTYDIALOG_H
#define PROPERTYDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QSpinBox>
#include "coloredit.h"
#include "baseshape.h"

class QLineEdit;

class PropertyDialog : public QDialog
{
    Q_OBJECT
public:
    explicit PropertyDialog(BaseShape *apShape,
                            const QString& asTitle = tr("Properties"),
                            QWidget* apParent = 0);
public slots:
    void accept();
private:
    QLineEdit* mpName;
    ColorEdit* mpTextColor;
    ColorEdit* mpPenColor;
    ColorEdit* mpBrushColor;
    QComboBox* mpPenStyle;
    QComboBox* mpBrushStyle;
    QSpinBox* mpPenWidth;
    QLineEdit* mpInfo;
    BaseShape* mpShape;
};

#endif // PROPERTYDIALOG_H
