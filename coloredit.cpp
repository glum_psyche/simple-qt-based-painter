/** @file coloredit.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "coloredit.h"
#include <QBoxLayout>
#include <QColorDialog>

ColorEdit::ColorEdit(const QString &asText, QWidget *apParent) :
    QWidget(apParent)
{
    mpText = new QLineEdit(asText);
    mpButtonColor = new QPushButton(tr("Choose..."));

    QHBoxLayout* vpHBoxLayout = new QHBoxLayout;
    vpHBoxLayout->addWidget(mpText);
    vpHBoxLayout->addWidget(mpButtonColor);

    setLayout(vpHBoxLayout);

    connect(mpButtonColor, SIGNAL(clicked()), this, SLOT(slotInputColor()));
}

void ColorEdit::setText(const QString &asText)
{
    mpText->setText(asText);
}

QString ColorEdit::text() const
{
    return mpText->text();
}

void ColorEdit::slotInputColor()
{
    QColor vColor = QColorDialog::getColor(mpText->text(), this);
    if(vColor.isValid())
    {
        mpText->setText(vColor.name());
    }
}
