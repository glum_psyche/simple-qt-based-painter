/** @file abstractshapebuilder.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef ABSTRACTSHAPEBUILDER_H
#define ABSTRACTSHAPEBUILDER_H

#include <QObject>

#include "baseshape.h"

class AbstractShapeBuilder : public QObject
{
    Q_OBJECT
public:
    explicit AbstractShapeBuilder(QObject *apParent = 0) : QObject(apParent) {}
    virtual ~AbstractShapeBuilder() {}
    virtual QString shapeType() = 0;
    virtual BaseShape* createShape(const ShapeData& aShapeData) = 0;
};

#endif // ABSTRACTSHAPEBUILDER_H
