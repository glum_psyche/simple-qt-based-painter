/** @file painterwindow.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef PAINTERWINDOW_H
#define PAINTERWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>

#include "baseshape.h"

class QGraphicsScene;
class QComboBox;
class ShapeFactory;

class PainterWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit PainterWindow(QWidget* apParent = 0);
    ~PainterWindow();
private:
    void createActions();
    void createMenus();
    void createToolBars();
    void setZValue(int anZValue);
    void setupShape(BaseShape* apShape, bool abMoveOnTop = true,
                    bool abAutoCoordinates = true);
    BaseShape* selectedShape() const;
    void addShape(BaseShape* apNewShape, bool abUpdate = true);
    void deleteShape(QGraphicsObject* apItem);
    void saveScene(const QString& asFileName);
    void openScene(const QString& asFileName);
    bool hasShapes() const;
    bool canSave() const;
    bool exitApp();
    void closeEvent(QCloseEvent* aEvent);
    bool isShapeHitInScene(const BaseShape* vpShape);
    void autoOpen();
private slots:
    void slotHasChanges();
    void slotNoChanges();
    void slotAddShape();
    void slotDeleteShape();
    void slotCutShape();
    void slotCopyShape();
    void slotPasteShape();
    void slotBringShapeToFront();
    void slotSendShapeToBack();
    void slotRaisePropertiesWindow();
    void slotRaisePropertiesWindow(BaseShape* apShape);
    void slotUpdateActions();
    void slotUpdateListBox();
    void slotSelectCurrentShape(int anIndex);
    void slotShowToolBar(bool abChecked);
    void slotNew();
    void slotSaveAs();
    void slotSave();
    void slotOpen();
    void slotAdjustViewPort(BaseShape* apShape);
private:
    ShapeFactory* mpShapeFactory;
    QGraphicsScene* mpScene;
    QGraphicsView* mpView;
    QComboBox* mpComboBox;
    QString msFileName;
    QString msDefaultFileName;
    QMenu* mpMenuFile;
    QMenu* mpMenuView;
    QMenu* mpMenuEdit;
    QToolBar* mpToolBarEdit;
    QAction* mpActionNewScene;
    QAction* mpActionSaveAs;
    QAction* mpActionSave;
    QAction* mpActionOpen;
    QAction* mpActionShowToolBar;
    QAction* mpActionAddShape;
    QAction* mpActionExit;
    QAction* mpActionCut;
    QAction* mpActionCopy;
    QAction* mpActionPaste;
    QAction* mpActionDelete;
    QAction* mpActionBringToFront;
    QAction* mpActionSendToBack;
    QAction* mpActionProperties;
    QAction* mpActionEnableAuto;
    int mnMinZ;
    int mnMaxZ;
    int mnSeqNumber;
    bool mbHasChanges;
    QRectF mSceneDefaultRect;
};

#endif // PAINTERWINDOW_H
