/** @file baseshape.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "baseshape.h"

#include <QtCore>
#include <QDebug>

BaseShape::BaseShape(const QString &asName,
                     const QString &asTypeName,
                     const QColor aTextColor, const QColor aPenColor,
                     const QColor aBrushColor, const Qt::PenStyle aPenStyle,
                     const Qt::BrushStyle aBrushStyle, const int anPenWidth,
                     const QString &asInfo, const QPointF aCoordinates,
                     QGraphicsObject *apParent) :
    QGraphicsObject(apParent)
{
    mData.msName = asName;
    mData.msTypeName = asTypeName;
    mData.mTextColor = aTextColor;
    mData.mBrushColor = aBrushColor;
    mData.mPenColor = aPenColor;
    mData.mPenStyle = aPenStyle;
    mData.mBrushStyle = aBrushStyle;
    mData.mnPenWidth = anPenWidth;
    mData.msInfo = asInfo;
    mData.mCoordinates = aCoordinates;
    prepareShape();
}

BaseShape::BaseShape(const BaseShape &aBaseShape) :
    QGraphicsObject(aBaseShape.parentObject()),
    mData(aBaseShape.mData)
{
    prepareShape();
}

BaseShape::BaseShape(const ShapeData &aShapeData, QGraphicsObject* apParent) :
    QGraphicsObject(apParent),
    mData(aShapeData)
{
    prepareShape();
    setZValue(aShapeData.mnZValue);
}

BaseShape& BaseShape::operator=(const BaseShape& apOther)
{
    mData = apOther.mData;
    prepareShape();
    setZValue(mData.mnZValue);
    return *this;
}

BaseShape::~BaseShape()
{
}

QPainterPath BaseShape::shape() const
{
    return QPainterPath();
}

QRectF BaseShape::boundingRect() const
{
    return QRectF();
}

void BaseShape::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

void BaseShape::setPath()
{
}

void BaseShape::prepareShape()
{
    setFlags(ItemIsMovable | ItemIsSelectable);
    connect(this, SIGNAL(moves(BaseShape*)), SLOT(setCoordinatesOnMove()));
    connect(this, SIGNAL(nameChanged()), SLOT(setPath()));
}

QRectF BaseShape::boundingRectGlobal() const
{
    QRectF vRect = boundingRect();
    vRect.adjust(pos().x(), pos().y(), pos().x(), pos().y());
    return vRect;
}

void BaseShape::actualizePos()
{
    setPos(mData.mCoordinates);
}

void BaseShape::setName(const QString &asName)
{
    prepareGeometryChange();
    mData.msName = asName;
    update();
    emit nameChanged();
}

void BaseShape::setTextColor(const QColor &aColor)
{
    mData.mTextColor = aColor;
    update();
}

void BaseShape::setBrushColor(const QColor &aColor)
{
    mData.mBrushColor = aColor;
    update();
}

void BaseShape::setPenColor(const QColor &aColor)
{
    mData.mPenColor = aColor;
    update();
}

QVariant BaseShape::itemChange(GraphicsItemChange aChange, const QVariant &aValue)
{
    emit changed();
    return QGraphicsObject::itemChange(aChange, aValue);
}

void BaseShape::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *)
{
    emit mouseDoubleClicked(this);
}

void BaseShape::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    emit moves(this);
    QGraphicsObject::mouseMoveEvent(event);
}

void BaseShape::setCoordinatesOnMove()
{
    mData.mCoordinates = this->pos();
}

const QColor &BaseShape::brushColor() const
{
    return mData.mBrushColor;
}

const QColor &BaseShape::penColor() const
{
    return mData.mPenColor;
}

const QColor &BaseShape::textColor() const
{
    return mData.mTextColor;
}

const QString& BaseShape::name() const
{
    return mData.msName;
}

const QString &BaseShape::typeName() const
{
    return mData.msTypeName;
}

void BaseShape::setPenStyle(const Qt::PenStyle aPenStyle)
{
    mData.mPenStyle = aPenStyle;
    update();
}

const Qt::PenStyle &BaseShape::penStyle() const
{
    return mData.mPenStyle;
}

void BaseShape::setBrushStyle(const Qt::BrushStyle aBrushStyle)
{
    mData.mBrushStyle = aBrushStyle;
    update();
}

const Qt::BrushStyle &BaseShape::brushStyle() const
{
    return mData.mBrushStyle;
}

void BaseShape::setPenWidth(const int anPenWidth)
{
    mData.mnPenWidth = anPenWidth;
    update();
}

int BaseShape::penWidth() const
{
    return mData.mnPenWidth;
}

void BaseShape::setInfo(const QString &asInfo)
{
    mData.msInfo = asInfo;
}

const QString &BaseShape::info() const
{
    return mData.msInfo;
}

void BaseShape::setCoordinates(const QPointF aCoordinates)
{
    mData.mCoordinates = aCoordinates;
}

const QPointF &BaseShape::coordinates() const
{
    return mData.mCoordinates;
}

const QString BaseShape::serialized() const
{
    QString vsSting = QString("Shape\t%1\t%2\t%3\t%4\t%5\t%6\t%7\t%8\t%9\t%10\t%11\t%12")
            .arg(this->typeName(), this->name(), this->textColor().name(),
                 this->penColor().name(), this->brushColor().name())
            .arg(this->penStyle())
            .arg(this->brushStyle())
            .arg(this->penWidth())
            .arg(this->info())
            .arg(this->coordinates().x())
            .arg(this->coordinates().y())
            .arg(this->zValue());
    qDebug() << qPrintable(vsSting);
    return vsSting;
}

void BaseShape::deserialize(const QString &asSerialized)
{
    qDebug() << qPrintable(asSerialized);
    putShape(toShapeData(asSerialized));
}

QMimeData* BaseShape::data() const
{
    QMimeData* vpMimeData = new QMimeData;
    qDebug() << serialized();
    vpMimeData->setText(serialized());
    vpMimeData->setData("application/shape", serialized().toUtf8());
    return vpMimeData;
}

void BaseShape::putShape(const ShapeData &aShape)
{
    mData.msName = aShape.msName;
    mData.mTextColor = aShape.mTextColor;
    mData.mBrushColor = aShape.mBrushColor;
    mData.mPenColor = aShape.mPenColor;
    mData.mPenStyle = aShape.mPenStyle;
    mData.mBrushStyle = aShape.mBrushStyle;
    mData.mnPenWidth = aShape.mnPenWidth;
    mData.msInfo = aShape.msInfo;
    mData.mCoordinates = aShape.mCoordinates;
}

void BaseShape::putData(const QByteArray& apData)
{
    putShape(toShapeData(apData));
}

void BaseShape::loadFromData(const QMimeData *apData)
{
    putShape(toShapeData(apData));
}

ShapeData BaseShape::toShapeData(const QMimeData* aData)
{
    return toShapeData(QString::fromUtf8(aData->data("application/shape").constData()));
}

ShapeData BaseShape::toShapeData(const QString &asText)
{
    ShapeData vData;
    QStringList vcParts = asText.split("\t");
    if(vcParts.count() >= 13 && vcParts.first() == "Shape")
    {
        vData.msTypeName = vcParts.at(1);
        vData.msName = vcParts.at(2);
        vData.mTextColor = vcParts.at(3);
        vData.mPenColor = vcParts.at(4);
        vData.mBrushColor = vcParts.at(5);
        vData.mPenStyle = static_cast<Qt::PenStyle>(vcParts.at(6).toInt());
        vData.mBrushStyle = static_cast<Qt::BrushStyle>(vcParts.at(7).toInt());
        vData.mnPenWidth = vcParts.at(8).toInt();
        vData.msInfo = vcParts.at(9);
        vData.mCoordinates = QPointF(vcParts.at(10).toFloat(), vcParts.at(11).toFloat());
        qDebug() << vData.mCoordinates.x() << ":" << vData.mCoordinates.y() << "\n";
        vData.mnZValue = vcParts.at(12).toInt();
    }
    else
    {
        vData.msTypeName = "UnknownShape";
    }
    return vData;
}
