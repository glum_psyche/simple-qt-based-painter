/** @file shapebuilder.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef SHAPEBUILDER_H
#define SHAPEBUILDER_H

#include "abstractshapebuilder.h"

template <class ShapeType>
class ShapeBuilder : public AbstractShapeBuilder
{
public:
    ShapeBuilder();
    virtual ~ShapeBuilder();
    virtual QString shapeType();
    virtual BaseShape* createShape(const ShapeData& aShapeData);
};

template <class ShapeType>
ShapeBuilder<ShapeType>::ShapeBuilder()
{
}

template <class ShapeType>
ShapeBuilder<ShapeType>::~ShapeBuilder()
{
}

template <class ShapeType>
QString ShapeBuilder<ShapeType>::shapeType()
{
    ShapeType vShape;
    return vShape.typeName();
}

template <class ShapeType>
BaseShape* ShapeBuilder<ShapeType>::createShape(const ShapeData& aShapeData)
{
    BaseShape* vpShape = new ShapeType(aShapeData);
    return vpShape;
}

#endif // SHAPEBUILDER_H
