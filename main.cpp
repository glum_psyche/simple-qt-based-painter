/** @file main.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include <QApplication>
#include "painterwindow.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    PainterWindow vPainterWindow;

    vPainterWindow.show();

    return app.exec();
}
