/** @file triangleshape.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "triangleshape.h"

TriangleShape::TriangleShape(const QString& asName,
                             QColor aTextColor, QColor aPenColor,
                             QColor aBrushColor, Qt::PenStyle aPenStyle,
                             Qt::BrushStyle aBrushStyle, int anPenWidth,
                             const QString &asInfo,
                             QPointF aCoordinates, QGraphicsObject *apParent) :
    BaseShape(asName, tr("Triangle"), aTextColor, aPenColor,
                  aBrushColor, aPenStyle, aBrushStyle, anPenWidth,
                  asInfo, aCoordinates, apParent)
{
    setPath();
}

TriangleShape::TriangleShape(const TriangleShape &aTriangleShape) :
    BaseShape(aTriangleShape.name(), tr("Triangle"), aTriangleShape.textColor(),
              aTriangleShape.penColor(), aTriangleShape.brushColor(),
              aTriangleShape.penStyle(), aTriangleShape.brushStyle(),
              aTriangleShape.penWidth(), aTriangleShape.info(),
              aTriangleShape.coordinates(),
              aTriangleShape.parentObject())
{
    setPath();
}

TriangleShape::TriangleShape(const BaseShape& aBaseShape) :
    BaseShape(aBaseShape.name(), tr("Ellipse"), aBaseShape.textColor(),
              aBaseShape.penColor(), aBaseShape.brushColor(),
              aBaseShape.penStyle(), aBaseShape.brushStyle(),
              aBaseShape.penWidth(), aBaseShape.info(),
              aBaseShape.coordinates(),
              aBaseShape.parentObject())
{
    setPath();
}

TriangleShape::TriangleShape(const ShapeData &aShapeData, QGraphicsObject *apParent) :
    BaseShape(aShapeData, apParent)
{
    setPath();
}

TriangleShape::~TriangleShape()
{
}

QRectF TriangleShape::boundingRect() const
{
    const int vnMargin = 1;
    return mPath.boundingRect().adjusted(-vnMargin, -vnMargin, +vnMargin, +vnMargin);
}

QPainterPath TriangleShape::shape() const
{
    QPolygonF vTriangle = mPath;
    QPainterPath vPath;
    vPath.addPolygon(vTriangle);
    return vPath;
}

void TriangleShape::paint(QPainter *apPainter, const QStyleOptionGraphicsItem *apOption, QWidget *)
{
    apPainter->save();
    QPen vPen(mData.mPenColor);
    if(apOption->state & QStyle::State_Selected)
    {
        vPen.setStyle(Qt::DotLine);
        vPen.setWidth(2);
    }
    else
    {
        vPen.setStyle(penStyle());
        vPen.setWidth(penWidth());
    }
    apPainter->setPen(vPen);
    apPainter->setBrush(QBrush(mData.mBrushColor, mData.mBrushStyle));
    QPolygonF vTriangle = mPath;
    apPainter->drawPolygon(vTriangle);
    apPainter->setPen(mData.mTextColor);
    QRectF vRect = vTriangle.boundingRect();
    vRect.adjust(0, vRect.height() / 3, 0, 0);
    apPainter->drawText(vRect, Qt::AlignCenter, mData.msName);
    apPainter->restore();
}

void TriangleShape::setPath()
{
    const int vnPadding = 8;
    QFontMetricsF vMetrics = qApp->fontMetrics();
    QRectF vRect = vMetrics.boundingRect(mData.msName);
    vRect.adjust(-vnPadding, -vnPadding, +vnPadding, +vnPadding);
    vRect.translate(-vRect.center());
    qreal vdX1, vdY1, vdX2, vdY2 = 0;
    vRect.getCoords(&vdX1, &vdY1, &vdX2, &vdY2);
    vdX1 -= vRect.width() / 2;
    vdX2 += vRect.width() / 2;
    vdY1 -= vRect.height() / 2;
    vdY2 += vRect.height();
    mPath = QPolygonF(
                QVector<QPointF>()
                << QPointF(vdX1, vdY2)
                << QPointF(vdX2, vdY2)
                << QPointF((vdX1 + vdX2) / 2, vdY1));
}
