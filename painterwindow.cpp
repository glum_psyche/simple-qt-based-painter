/** @file painterwindow.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "painterwindow.h"

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QComboBox>
#include <QDebug>
#include <QDir>
#include <QClipboard>
#include <QMimeData>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include <QToolBar>
#include <QMenuBar>
#include <QLabel>

#include "createdialog.h"
#include "propertydialog.h"

#include "shapefactory.h"
#include "ellipseshape.h"
#include "rectangleshape.h"
#include "triangleshape.h"

PainterWindow::PainterWindow(QWidget *apParent) :
    QMainWindow(apParent),
    mSceneDefaultRect(0, 0, 600, 500)
{
    mbHasChanges = false;
    msDefaultFileName = qApp->applicationDirPath()
            + QDir::toNativeSeparators("/default.dat");

    ShapeFactory::instance()->registerShape<EllipseShape>();
    ShapeFactory::instance()->registerShape<RectangleShape>();
    ShapeFactory::instance()->registerShape<TriangleShape>();

    mpScene = new QGraphicsScene(mSceneDefaultRect, this);
    mpView = new QGraphicsView(mpScene, this);
    mpView->setViewport(new QWidget(this));
    mpView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    mpView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    mpView->setDragMode(QGraphicsView::RubberBandDrag);
    mpView->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    mpView->setContextMenuPolicy(Qt::ActionsContextMenu);

    setCentralWidget(mpView);

    setWindowTitle(tr("Painter"));
    createActions();
    createMenus();
    createToolBars();

    connect(mpScene, SIGNAL(selectionChanged()), this,
            SLOT(slotUpdateActions()));
    connect(mpComboBox, SIGNAL(activated(int)), this,
            SLOT(slotSelectCurrentShape(int)));
    connect(mpToolBarEdit, SIGNAL(visibilityChanged(bool)),
            mpActionShowToolBar, SLOT(setChecked(bool)));

    slotNew();
    autoOpen();
}

PainterWindow::~PainterWindow()
{
    slotNew();
    ShapeFactory::destruct();
}

void PainterWindow::closeEvent(QCloseEvent *aEvent)
{
    if(exitApp())
    {
        aEvent->accept();
    }
    else
    {
        aEvent->ignore();
    }
}

bool PainterWindow::hasShapes() const
{
    return !mpScene->items().isEmpty();
}

bool PainterWindow::canSave() const
{
    return QFile::exists(msFileName) && mbHasChanges && hasShapes();
}

void PainterWindow::slotUpdateActions()
{
    qDebug("update actions");
    bool vbIsShapeSelected = (selectedShape() != 0);
    bool vbIsCopied = qApp->clipboard()->mimeData()->hasText();
    mpActionOpen->setEnabled(!mpActionEnableAuto->isChecked());
    mpActionSave->setEnabled(canSave() && !mpActionEnableAuto->isChecked());
    mpActionSaveAs->setEnabled(hasShapes() && !mpActionEnableAuto->isChecked());
    mpActionCut->setEnabled(vbIsShapeSelected);
    mpActionCopy->setEnabled(vbIsShapeSelected);
    mpActionPaste->setEnabled(vbIsCopied);
    mpActionDelete->setEnabled(vbIsShapeSelected);
    mpActionBringToFront->setEnabled(vbIsShapeSelected);
    mpActionSendToBack->setEnabled(vbIsShapeSelected);
    mpActionProperties->setEnabled(vbIsShapeSelected);
    foreach(QAction* vpAction, mpView->actions())
        mpView->removeAction(vpAction);
    foreach(QAction* vpAction, mpMenuEdit->actions())
    {
        if(vpAction->isEnabled())
            mpView->addAction(vpAction);
    }
    foreach(QAction* vpAction, mpMenuView->actions())
    {
        if(vpAction->isEnabled())
            mpView->addAction(vpAction);
    }
    if(vbIsShapeSelected)
    {
        for(int i = 0; i < mpComboBox->count(); ++i)
        {
            if(mpScene->items().at(mpComboBox->itemData(i).toInt())->isSelected())
                mpComboBox->setCurrentIndex(i);
        }
    }
    else
    {
        mpComboBox->setCurrentIndex(-1);
    }
}

void PainterWindow::saveScene(const QString &asFileName)
{
    QString vsFileName = asFileName;
    if(vsFileName.right(4) != ".txt" && vsFileName.right(4) != ".dat")
        vsFileName += ".dat";

    QFile vFile(vsFileName);

    if(vFile.open(QIODevice::WriteOnly))
    {
        QTextStream vStream(&vFile);
        foreach(QGraphicsItem* vpShape, mpScene->items())
        {
            BaseShape* vAnyShape = qgraphicsitem_cast<BaseShape*>(vpShape);
            vStream << vAnyShape->serialized() << "\n";
            qDebug("s: %s", vAnyShape->serialized().toStdString().c_str());
        }
        vFile.close();
        if(vStream.status() != QTextStream::Ok)
        {
            qDebug("File writing error!");
        }
        msFileName = vsFileName;
        slotUpdateActions();
        slotNoChanges();
    }
}

void PainterWindow::openScene(const QString &asFileName)
{
    QFile vFile(asFileName);
    qDebug("filename: %s", asFileName.toStdString().c_str());
    if(vFile.open(QIODevice::ReadOnly))
    {
        QTextStream vStream(&vFile);
        QString vsString;
        while(!vStream.atEnd())
        {
            vsString = vStream.readLine();
            qDebug("line: %s", vsString.toStdString().c_str());
            addShape(ShapeFactory::instance()->createShapeFromSerialized(vsString),
                     false);
        }
        slotUpdateActions();
        slotUpdateListBox();
        if(vStream.status() != QTextStream::Ok)
        {
            qDebug("File reading error!");
        }
        vFile.close();
        msFileName = asFileName;
        if(mpScene->itemsBoundingRect().width() < mSceneDefaultRect.width()
                && mpScene->itemsBoundingRect().height() < mSceneDefaultRect.height())
        {
            mpView->setSceneRect(mSceneDefaultRect);
        }
        else
        {
            mpView->setSceneRect(mpScene->itemsBoundingRect());
        }
        slotNoChanges();
    }
}

void PainterWindow::slotNew()
{
    if(mbHasChanges)
    {
        QMessageBox::StandardButton vButton = QMessageBox::question(
                    this, tr("New scene"),
                    tr("Here some unsaved changes!\n"
                       "Would you like to save them? "
                       "If you answer 'No' changes will be lost!"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        if(vButton == QMessageBox::Yes)
        {
            slotSaveAs();
        }
        else
        if(vButton == QMessageBox::Cancel)
        {
            return;
        }
    }
    if(!mpScene->items().isEmpty())
    {
        qDeleteAll(mpScene->items());
        mpScene->clear();
        mpScene->update();
    }
    slotAdjustViewPort(0);
    mnMinZ = 0;
    mnMaxZ = 0;
    mnSeqNumber = 0;
    msFileName.clear();
    slotUpdateActions();
    slotNoChanges();
}

void PainterWindow::slotSave()
{
    saveScene(msFileName);
}

void PainterWindow::slotSaveAs()
{
    QString vsString = QFileDialog::getSaveFileName(
                this, tr("Save file"), qApp->applicationDirPath(),
                tr("Text(*.txt *.dat);;All files(*.*)"));
    if(!vsString.isEmpty())
    {
        saveScene(vsString);
    }
}

void PainterWindow::slotOpen()
{
    QString vsString = QFileDialog::getOpenFileName(
                this, tr("Open file"), qApp->applicationDirPath(),
                tr("Text(*.txt *.dat);;All files(*.*)"));
    if(!vsString.isEmpty())
    {
        slotNew();
        openScene(vsString);
    }
}

bool PainterWindow::isShapeHitInScene(const BaseShape *vpShape)
{
    return (mpScene->sceneRect().left() <= vpShape->boundingRectGlobal().left())
            && (mpScene->sceneRect().top() <= vpShape->boundingRectGlobal().top())
            && (mpScene->sceneRect().right() >= vpShape->boundingRectGlobal().right())
            && (mpScene->sceneRect().bottom() >= vpShape->boundingRectGlobal().bottom());
}

void PainterWindow::slotAdjustViewPort(BaseShape *apShape)
{
    if(!apShape ||
            (mpScene->width() < mSceneDefaultRect.width()
            && mpScene->height() < mSceneDefaultRect.height()))
    {
        mpView->setSceneRect(mSceneDefaultRect);
    }
    else
    if(!isShapeHitInScene(apShape))
    {
        mpView->setSceneRect(mpScene->itemsBoundingRect());
        qDebug("shape moves out");
    }
}

void PainterWindow::slotSelectCurrentShape(int anIndex)
{
    int vnIndex = mpComboBox->itemData(anIndex).toInt();
    if(mpScene->items().count() > 0)
    {
        for(int i = 0; i < mpScene->items().count(); ++i)
        {
            mpScene->items().at(i)->setSelected(vnIndex == i);
        }
    }
}

void PainterWindow::slotHasChanges()
{
    mbHasChanges = true;
}

void PainterWindow::slotNoChanges()
{
    mbHasChanges = false;
}

void PainterWindow::slotUpdateListBox()
{
    qDebug("update list box");
    mpComboBox->clear();
    for(int i = 0; i < mpScene->items().count(); ++i)
    {
        mpComboBox->addItem(
                    qgraphicsitem_cast<BaseShape*>(mpScene->items().at(i))->name(),
                    QVariant::fromValue(i));
    }
    slotHasChanges();
}

void PainterWindow::addShape(BaseShape *apNewShape, bool abUpdate)
{
    if(apNewShape)
    {
        connect(apNewShape, SIGNAL(nameChanged()), this, SLOT(slotUpdateListBox()));
        connect(apNewShape, SIGNAL(destroyed()), this, SLOT(slotUpdateListBox()));
        connect(apNewShape, SIGNAL(moves(BaseShape*)), this, SLOT(slotHasChanges()));
        connect(apNewShape, SIGNAL(mouseDoubleClicked(BaseShape*)),
                this, SLOT(slotRaisePropertiesWindow(BaseShape*)));
        connect(apNewShape, SIGNAL(moves(BaseShape*)), this, SLOT(slotAdjustViewPort(BaseShape*)));
        setupShape(apNewShape, abUpdate, abUpdate);
        if(abUpdate)
        {
            slotUpdateListBox();
        }
    }
}

void PainterWindow::slotAddShape()
{
    QList<CreateDialog::ShapeChoice> vcShapes;
    QStringList vcTypes = ShapeFactory::instance()->types();
    foreach(QString vpKey, vcTypes)
    {
        vcShapes << CreateDialog::ShapeChoice(vpKey, vpKey);
    }

    CreateDialog* vpCreateDialog = new CreateDialog(tr("New shape %1")
                                                    .arg(mnSeqNumber + 1),
                                                    vcShapes);
    if(vpCreateDialog->exec())
    {
        ShapeData vData;

        vData.msName = vpCreateDialog->name();
        vData.msTypeName = vpCreateDialog->choice();
        vData.mTextColor = QColor(Qt::darkGreen);
        vData.mPenColor = QColor(Qt::darkBlue);
        vData.mBrushColor = QColor(Qt::white);
        vData.mPenStyle = Qt::SolidLine;
        vData.mBrushStyle = Qt::SolidPattern;
        vData.mnPenWidth = 1;
        vData.msInfo= "";

        addShape(ShapeFactory::instance()->createShape(vData));
    }

    delete vpCreateDialog;
}

void PainterWindow::setupShape(BaseShape *apShape, bool abMoveOnTop,
                               bool abAutoCoordinates)
{
    if(abAutoCoordinates)
    {
        apShape->setCoordinates(QPoint(80 + (100 * (mnSeqNumber % 5)),
                                       80 + (50 * ((mnSeqNumber / 5) % 7))));
    }
    apShape->actualizePos();
    if(apShape->zValue() > mnMaxZ)
    {
        mnMaxZ = apShape->zValue();
    }
    else
    if(apShape->zValue() < mnMinZ)
    {
        mnMinZ = apShape->zValue();
    }
    mpScene->addItem(apShape);
    ++mnSeqNumber;
    if(abMoveOnTop)
    {
        mpScene->clearSelection();
        apShape->setSelected(true);
        slotBringShapeToFront();
    }
}

BaseShape* PainterWindow::selectedShape() const
{
    QList<QGraphicsItem*> vcItems = mpScene->selectedItems();
    if(vcItems.count() == 1)
    {
        return qgraphicsitem_cast<BaseShape*>(vcItems.first());
    }
    else
    {
        return 0;
    }
}

void PainterWindow::deleteShape(QGraphicsObject *apItem)
{
    qDebug() << qPrintable(QString("delete shape: %1")
                      .arg(qgraphicsitem_cast<BaseShape*>(apItem)->name()));
    mpScene->removeItem(apItem);
    delete apItem;
}

void PainterWindow::slotDeleteShape()
{
    deleteShape(selectedShape());
}

void PainterWindow::slotRaisePropertiesWindow()
{
    BaseShape* vpShape = selectedShape();
    slotRaisePropertiesWindow(vpShape);
}

void PainterWindow::slotRaisePropertiesWindow(BaseShape* apShape)
{
    PropertyDialog* vpPropertyDialog = new PropertyDialog(
                apShape, tr("Properties: %1").arg(apShape->typeName()));
    vpPropertyDialog->exec();
    delete vpPropertyDialog;
}

void PainterWindow::slotCutShape()
{
    BaseShape* vpShape = selectedShape();
    if(!vpShape)
        return;
    slotCopyShape();
    deleteShape(vpShape);
}

void PainterWindow::slotCopyShape()
{
    BaseShape* vpShape = selectedShape();
    if(!vpShape)
        return;
    qApp->clipboard()->setMimeData(vpShape->data());
    qDebug() << qApp->clipboard()->mimeData()->text();
    slotUpdateActions();
}

void PainterWindow::slotPasteShape()
{
    addShape(ShapeFactory::instance()->createShapeFromMimeData(
                 qApp->clipboard()->mimeData()));
}

void PainterWindow::autoOpen()
{
    if(mpActionEnableAuto->isChecked() && QFile::exists(msDefaultFileName))
    {
        qDebug("open: %s", msDefaultFileName.toStdString().c_str());
        openScene(msDefaultFileName);
    }
}

void PainterWindow::setZValue(int anZValue)
{
    BaseShape* vpShape = selectedShape();
    if(vpShape)
        vpShape->setZValue(anZValue);
}

void PainterWindow::slotBringShapeToFront()
{
    setZValue(++mnMaxZ);
}

void PainterWindow::slotSendShapeToBack()
{
    setZValue(--mnMinZ);
}

void PainterWindow::slotShowToolBar(bool abChecked)
{
    mpToolBarEdit->setVisible(abChecked);
}

bool PainterWindow::exitApp()
{
    if(mpActionEnableAuto->isChecked())
    {
        saveScene(msDefaultFileName);
        qDebug("save: %s", msDefaultFileName.toStdString().c_str());
        return true;
    }
    else
    if(mbHasChanges)
    {
        QMessageBox::StandardButton vAnswer = QMessageBox::question(
                    this, tr("Quit application"),
                    tr("Application has some unsaved changes!\nWould you like to save them?"),
                    QMessageBox::Save | QMessageBox::No | QMessageBox::Cancel);
        if(vAnswer == QMessageBox::Cancel)
        {
            return false;
        }
        else
        {
            if(vAnswer == QMessageBox::Save)
            {
                if(canSave())
                {
                    slotSave();
                }
                else
                {
                    slotSaveAs();
                }
            }
            return true;
        }
    }
    else
        return true;
}

void PainterWindow::createActions()
{
    mpActionShowToolBar = new QAction(tr("Show toolbar"), this);
    mpActionShowToolBar->setText(tr("&Show toolbar"));
    mpActionShowToolBar->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_H));
    mpActionShowToolBar->setToolTip(tr("Show toolbar"));
    mpActionShowToolBar->setStatusTip(tr("Show toolbar"));
    mpActionShowToolBar->setWhatsThis(tr("Show toolbar"));
    mpActionShowToolBar->setCheckable(true);
    connect(mpActionShowToolBar, SIGNAL(triggered(bool)), SLOT(slotShowToolBar(bool)));

    mpActionNewScene = new QAction(tr("New scene"), this);
    mpActionNewScene->setText(tr("&New scene"));
    mpActionNewScene->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
    mpActionNewScene->setToolTip(tr("New scene"));
    mpActionNewScene->setStatusTip(tr("New scene"));
    mpActionNewScene->setWhatsThis(tr("New scene"));
    connect(mpActionNewScene, SIGNAL(triggered()), SLOT(slotNew()));

    mpActionOpen = new QAction(tr("Open shapes"), this);
    mpActionOpen->setText(tr("&Open shapes"));
    mpActionOpen->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
    mpActionOpen->setToolTip(tr("Open shapes"));
    mpActionOpen->setStatusTip(tr("Open shapes"));
    mpActionOpen->setWhatsThis(tr("Open shapes"));
    connect(mpActionOpen, SIGNAL(triggered()), SLOT(slotOpen()));

    mpActionSave = new QAction(tr("Save shapes"), this);
    mpActionSave->setText(tr("&Save shapes"));
    mpActionSave->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
    mpActionSave->setToolTip(tr("Save shapes"));
    mpActionSave->setStatusTip(tr("Save shapes"));
    mpActionSave->setWhatsThis(tr("Save shapes"));
    connect(mpActionSave, SIGNAL(triggered()), SLOT(slotSave()));

    mpActionSaveAs = new QAction(tr("Save shapes as..."), this);
    mpActionSaveAs->setText(tr("Save shapes &as..."));
    mpActionSaveAs->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));
    mpActionSaveAs->setToolTip(tr("Save shapes as..."));
    mpActionSaveAs->setStatusTip(tr("Save shapes as..."));
    mpActionSaveAs->setWhatsThis(tr("Save shapes as..."));
    connect(mpActionSaveAs, SIGNAL(triggered()), SLOT(slotSaveAs()));

    mpActionAddShape = new QAction(tr("New shape"), this);
    mpActionAddShape->setText(tr("New &shape"));
    mpActionAddShape->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_E));
    mpActionAddShape->setToolTip(tr("New shape"));
    mpActionAddShape->setStatusTip(tr("New shape"));
    mpActionAddShape->setWhatsThis(tr("New shape"));
    connect(mpActionAddShape, SIGNAL(triggered()), SLOT(slotAddShape()));

    mpActionCut = new QAction(tr("Cut"), this);
    mpActionCut->setText(tr("C&ut"));
    mpActionCut->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_X));
    mpActionCut->setToolTip(tr("Cut shape"));
    mpActionCut->setStatusTip(tr("Cut shape"));
    mpActionCut->setWhatsThis(tr("Cut shape"));
    connect(mpActionCut, SIGNAL(triggered()), SLOT(slotCutShape()));

    mpActionCopy = new QAction(tr("Copy"), this);
    mpActionCopy->setText(tr("&Copy"));
    mpActionCopy->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_C));
    mpActionCopy->setToolTip(tr("Copy shape"));
    mpActionCopy->setStatusTip(tr("Copy shape"));
    mpActionCopy->setWhatsThis(tr("Copy shape"));
    connect(mpActionCopy, SIGNAL(triggered()), SLOT(slotCopyShape()));

    mpActionPaste = new QAction(tr("Paste"), this);
    mpActionPaste->setText(tr("&Paste"));
    mpActionPaste->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_V));
    mpActionPaste->setToolTip(tr("Paste shape"));
    mpActionPaste->setStatusTip(tr("Paste shape"));
    mpActionPaste->setWhatsThis(tr("Paste items"));
    connect(mpActionPaste, SIGNAL(triggered()), SLOT(slotPasteShape()));

    mpActionDelete = new QAction(tr("Delete"), this);
    mpActionDelete->setText(tr("&Delete"));
    mpActionDelete->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Delete));
    mpActionDelete->setToolTip(tr("Delete shape"));
    mpActionDelete->setStatusTip(tr("Delete shape"));
    mpActionDelete->setWhatsThis(tr("Delete shape"));
    connect(mpActionDelete, SIGNAL(triggered()), SLOT(slotDeleteShape()));

    mpActionBringToFront = new QAction(tr("To front"), this);
    mpActionBringToFront->setText(tr("To &front"));
    mpActionBringToFront->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_F));
    mpActionBringToFront->setToolTip(tr("Bring shape to front"));
    mpActionBringToFront->setStatusTip(tr("Bring shape to front"));
    mpActionBringToFront->setWhatsThis(tr("Bring shape to front"));
    connect(mpActionBringToFront, SIGNAL(triggered()), SLOT(slotBringShapeToFront()));

    mpActionSendToBack = new QAction(tr("To back"), this);
    mpActionSendToBack->setText(tr("To &back"));
    mpActionSendToBack->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_B));
    mpActionSendToBack->setToolTip(tr("Send shape to back"));
    mpActionSendToBack->setStatusTip(tr("Send shape to back"));
    mpActionSendToBack->setWhatsThis(tr("Send shape to back"));
    connect(mpActionSendToBack, SIGNAL(triggered()), SLOT(slotSendShapeToBack()));

    mpActionProperties = new QAction(tr("Properties"), this);
    mpActionProperties->setText(tr("Prop&erties"));
    mpActionProperties->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_P));
    mpActionProperties->setToolTip(tr("Shape properties"));
    mpActionProperties->setStatusTip(tr("Shape properties"));
    mpActionProperties->setWhatsThis(tr("Shape properties"));
    connect(mpActionProperties, SIGNAL(triggered()), SLOT(slotRaisePropertiesWindow()));

    mpActionEnableAuto = new QAction(tr("Enable auto open/save"), this);
    mpActionEnableAuto->setText(tr("Enable &auto open/save"));
    mpActionEnableAuto->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_A));
    mpActionEnableAuto->setToolTip(tr("Enable auto open/save"));
    mpActionEnableAuto->setStatusTip(tr("Enable auto open/save"));
    mpActionEnableAuto->setWhatsThis(tr("Enable auto open/save"));
    mpActionEnableAuto->setCheckable(true);
    mpActionEnableAuto->setChecked(true);
    connect(mpActionEnableAuto, SIGNAL(triggered()), SLOT(slotUpdateActions()));

    mpActionExit = new QAction(tr("Exit"), this);
    mpActionExit->setText(tr("&Exit"));
    mpActionExit->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));
    mpActionExit->setToolTip(tr("Exit application"));
    mpActionExit->setStatusTip(tr("Exit application"));
    mpActionExit->setWhatsThis(tr("Exit application"));
    connect(mpActionExit, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
}

void PainterWindow::createMenus()
{
    mpMenuFile = new QMenu(tr("&File"), this);
    mpMenuFile->addAction(mpActionNewScene);
    mpMenuFile->addAction(mpActionOpen);
    mpMenuFile->addAction(mpActionSave);
    mpMenuFile->addAction(mpActionSaveAs);
    mpMenuFile->addSeparator();
    mpMenuFile->addAction(mpActionEnableAuto);
    mpMenuFile->addSeparator();
    mpMenuFile->addAction(mpActionExit);
    menuBar()->addMenu(mpMenuFile);

    mpMenuView = new QMenu(tr("&View"), this);
    mpMenuView->addSeparator();
    mpMenuView->addAction(mpActionShowToolBar);
    menuBar()->addMenu(mpMenuView);

    mpMenuEdit = new QMenu(tr("&Edit"), this);

    mpMenuEdit->addAction(mpActionAddShape);
    mpMenuEdit->addAction(mpActionDelete);
    mpMenuEdit->addSeparator();
    mpMenuEdit->addAction(mpActionCopy);
    mpMenuEdit->addAction(mpActionPaste);
    mpMenuEdit->addAction(mpActionCut);
    mpMenuEdit->addSeparator();
    mpMenuEdit->addAction(mpActionBringToFront);
    mpMenuEdit->addAction(mpActionSendToBack);
    mpMenuEdit->addSeparator();
    mpMenuEdit->addAction(mpActionProperties);
    menuBar()->addMenu(mpMenuEdit);
}

void PainterWindow::createToolBars()
{
    mpToolBarEdit = new QToolBar(tr("Edit Operations"), this);
    mpComboBox = new QComboBox;
    QLabel* vpLabel = new QLabel(tr("Current item:"));
    vpLabel->setBuddy(mpComboBox);
    mpToolBarEdit->addWidget(vpLabel);
    mpToolBarEdit->addWidget(mpComboBox);
    mpToolBarEdit->addAction(mpActionAddShape);
    mpToolBarEdit->addAction(mpActionDelete);
    mpToolBarEdit->addSeparator();
    mpToolBarEdit->addAction(mpActionCopy);
    mpToolBarEdit->addAction(mpActionPaste);
    mpToolBarEdit->addAction(mpActionCut);
    mpToolBarEdit->addSeparator();
    mpToolBarEdit->addAction(mpActionBringToFront);
    mpToolBarEdit->addAction(mpActionSendToBack);
    mpToolBarEdit->addSeparator();
    mpToolBarEdit->addAction(mpActionProperties);
    addToolBar(Qt::LeftToolBarArea, mpToolBarEdit);
}
