/** @file rectangleshape.cpp
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#include "rectangleshape.h"

RectangleShape::RectangleShape(const QString& asName,
                               const QColor aTextColor,
                               const QColor aPenColor,
                               const QColor aBrushColor,
                               const Qt::PenStyle aPenStyle,
                               const Qt::BrushStyle aBrushStyle,
                               const int anPenWidth,
                               const QString &asInfo,
                               const QPointF aCoordianates,
                               QGraphicsObject *apParent) :
    BaseShape(asName, tr("Rectangle"), aTextColor, aPenColor,
                  aBrushColor, aPenStyle, aBrushStyle, anPenWidth,
                  asInfo, aCoordianates, apParent)
{    
    setPath();
}

RectangleShape::RectangleShape(const RectangleShape &aRectangleShape) :
    BaseShape(aRectangleShape.name(), tr("Rectangle"), aRectangleShape.textColor(),
              aRectangleShape.penColor(), aRectangleShape.brushColor(),
              aRectangleShape.penStyle(), aRectangleShape.brushStyle(),
              aRectangleShape.penWidth(), aRectangleShape.info(),
              aRectangleShape.coordinates(),
              aRectangleShape.parentObject())
{
    setPath();
}

RectangleShape::RectangleShape(const BaseShape& aBaseShape) :
    BaseShape(aBaseShape.name(), tr("Rectangle"), aBaseShape.textColor(),
              aBaseShape.penColor(), aBaseShape.brushColor(),
              aBaseShape.penStyle(), aBaseShape.brushStyle(),
              aBaseShape.penWidth(), aBaseShape.info(),
              aBaseShape.coordinates(),
              aBaseShape.parentObject())
{
    setPath();
}

RectangleShape::RectangleShape(const ShapeData &aShapeData,
                               QGraphicsObject *apParent) :
    BaseShape(aShapeData, apParent)
{
    setPath();
}

RectangleShape::~RectangleShape()
{
}

QRectF RectangleShape::boundingRect() const
{
    const int vnMargin = 1;
    return mPath.adjusted(-vnMargin, -vnMargin, +vnMargin, +vnMargin);
}

QPainterPath RectangleShape::shape() const
{
    QRectF vRect = mPath;
    QPainterPath vPath;
    vPath.addRect(vRect);
    return vPath;
}

void RectangleShape::paint(QPainter *apPainter, const QStyleOptionGraphicsItem *apOption, QWidget *)
{
    apPainter->save();
    QPen vPen(mData.mPenColor);
    if(apOption->state & QStyle::State_Selected)
    {
        vPen.setStyle(Qt::DotLine);
        vPen.setWidth(2);
    }
    else
    {
        vPen.setStyle(penStyle());
        vPen.setWidth(penWidth());
    }
    apPainter->setPen(vPen);
    apPainter->setBrush(QBrush(mData.mBrushColor, mData.mBrushStyle));
    QRectF vRect = mPath;
    apPainter->drawRect(vRect);
    apPainter->setPen(mData.mTextColor);
    apPainter->drawText(vRect, Qt::AlignCenter, mData.msName);
    apPainter->restore();
}

void RectangleShape::setPath()
{
    const int vnPadding = 8;
    QFontMetricsF vMetrics = qApp->fontMetrics();
    mPath = vMetrics.boundingRect(mData.msName);
    mPath.adjust(-vnPadding, -vnPadding, +vnPadding, +vnPadding);
    mPath.translate(-mPath.center());
}
