/** @file coloredit.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef COLOREDIT_H
#define COLOREDIT_H

#include <QLineEdit>
#include <QPushButton>

class QWidget;

class ColorEdit : public QWidget
{
    Q_OBJECT
public:
    explicit ColorEdit(const QString& asText = "", QWidget *apParent = 0);
    QString text() const;
    void setText(const QString& asText);
private slots:
    void slotInputColor();
private:
    QLineEdit* mpText;
    QPushButton* mpButtonColor;
};

#endif // COLOREDIT_H
