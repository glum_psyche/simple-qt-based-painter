# Copyright Evgeni Biryukov, 2012. All rights reserved.


TEMPLATE = app

greaterThan(QT_MAJOR_VERSION, 4): {
QT  += widgets
}

TARGET = Painter
DESTDIR = bin
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build
RCC_DIR = build

TEMPLATE = app

SOURCES += \
    main.cpp \
    baseshape.cpp \
    painterwindow.cpp \
    rectangleshape.cpp \
    ellipseshape.cpp \
    propertydialog.cpp \
    triangleshape.cpp \
    createdialog.cpp \
    coloredit.cpp \
    shapefactory.cpp

HEADERS += \
    baseshape.h \
    painterwindow.h \
    rectangleshape.h \
    ellipseshape.h \
    propertydialog.h \
    triangleshape.h \
    createdialog.h \
    coloredit.h \
    shapefactory.h \
    shapebuilder.h \
    abstractshapebuilder.h
