/** @file createdialog.h
 *
 *
 *  $Author$
 *  $Date$
 *  $Rev$
 *
 * Copyright Evgeni Biryukov, 2012. All rights reserved.
 */

#ifndef CREATEDIALOG_H
#define CREATEDIALOG_H

#include <QMap>
#include <QPair>
#include <QDialog>

class QRadioButton;
class QLineEdit;

class CreateDialog : public QDialog
{
    Q_OBJECT
public:
    typedef QPair<QString, QString> ShapeChoice;
    explicit CreateDialog(QString asDefaultName,
                          QList<ShapeChoice> acShapes,
                          QWidget *apParent = 0);
    const QString choice() const;
    QString name() const;
private:
    QMap<QString, QRadioButton*> mcRadioButtons;
    QLineEdit* mpName;
};

#endif // CREATEDIALOG_H
